(function () {
	
	var app = angular.module('TaskerApp');
	
	var taskController = function($scope) {
		
		var onAddTaskClick = function() {
			alert('Ready');
		};
		$scope.onAddTaskClick = onAddTaskClick;
		
	};
	
	app.directive('addTaskDemoPanel', function() {
		return {
			restrict: 'E',
			templateUrl: '/view/addTaskDemoDirective.html',
			controller: taskController
		};
	});
	
})();
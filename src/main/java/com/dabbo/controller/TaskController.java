package com.dabbo.controller;

import com.dabbo.database.respository.TaskRepository;
import com.dabbo.factory.TaskFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/status")
    public String status() {
        return "Task Controller.";
    }

    @GetMapping("/add")
    public String addNewTask(@RequestParam String taskType) {
        taskRepository.save(TaskFactory.createTask(taskType));
        return "Saved OK";
    }

}

package com.dabbo.factory;

import com.dabbo.database.entity.Task;

import static com.dabbo.database.entity.TaskType.ADDITION;
import static com.dabbo.database.entity.TaskType.SUBTRACTION;

public class TaskFactory {

    private TaskFactory() {
    }

    public static Task createTask(String taskType) {

        Task task;
        switch (taskType) {
            case "ADDITION":
                task = new Task(ADDITION);
                break;
            case "SUBTRACTION":
                task = new Task(SUBTRACTION);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return task;
    }

}

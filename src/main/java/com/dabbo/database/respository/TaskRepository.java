package com.dabbo.database.respository;

import com.dabbo.database.entity.Task;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<Task, Integer> {



}

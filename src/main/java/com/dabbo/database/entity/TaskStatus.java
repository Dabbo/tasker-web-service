package com.dabbo.database.entity;

public enum TaskStatus {

    QUEUED,
    PROCESSING,
    STOPPED,
    COMPLETE,
    DELETED

}

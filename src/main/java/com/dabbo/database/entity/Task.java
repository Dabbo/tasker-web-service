package com.dabbo.database.entity;

import com.dabbo.database.Taskable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Task implements Taskable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private TaskStatus taskStatus;

    private TaskType taskType;

    private Date createdTime;

    private Date lastModified;

    public Task(TaskType taskType) {
        this.taskStatus = TaskStatus.QUEUED;
        this.taskType = taskType;
        this.createdTime = new Date(new java.util.Date().getTime());
        this.lastModified = new Date(new java.util.Date().getTime());
    }

    public Integer getId() {
        return id;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}

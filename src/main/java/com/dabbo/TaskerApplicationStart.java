package com.dabbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskerApplicationStart {

    public static void main(String[] args) {
        SpringApplication.run(TaskerApplicationStart.class, args);
    }

}
